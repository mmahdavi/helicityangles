# Helicity angles analysis in VBF and VH topology

## Required packages

Before building this repository, the `MELA` package must have been installed w.r.t. the proper [`LCG`](http://lcginfo.cern.ch) environment.

For `MELA` package
```sh
. ./env.sh
git clone https://github.com/JHUGen/JHUGenMELA.git
cd JHUGenMELA
git checkout 00cc82efec77a8dbc7c908f4f8203e5693e20e97
./setup.sh -j $(nproc) standalone
cd $HELICITYANGLES_BASE
```

## Computing environment and building

At the start of each session, set up the environment with

```sh
. ./env.sh
```

Build the package with the following commands:

```sh
mkdir build
cd build
cmake ..
make -j $(nproc)
```

## Running interactively

```sh
computeAngles --verbose --first=FIRTS_EVRNT --last=LAST_EVENT --output=OUTPUT.root INPUFILES
```

