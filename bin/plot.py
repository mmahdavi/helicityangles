#!/usr/bin/env python 

import ROOT
import numpy as np
from sys import argv
import os
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import rc
from matplotlib.transforms import Bbox
import matplotlib.patches as mpl_patches
from array import array

def get_ZH(tag):
  return (data[tag]['isZH'] == 1) & (data[tag]['mjj'] > 80) & (data[tag]['mjj'] < 100) #& (data[tag]['detajj'] < 3)

def get_WH(tag):
  return (data[tag]['isWH'] == 1) & (data[tag]['mjj'] > 70) & (data[tag]['mjj'] < 90.) #& (data[tag]['detajj'] < 3)

def get_VBF(tag):
  return (data[tag]['mjj'] > 130)

def get_ONSHELL(tag):
  return (data[tag]['m4l'] > 124.95) & (data[tag]['m4l'] < 125.05)

def get_OFFSHELL(tag):
  return (data[tag]['m4l'] > 250) & (data[tag]['m4l'] < 500)


cuts_state, hypo = argv[1], argv[2]
if hypo not in ('a1', 'ps', 'bkg'):
  print('Please enter correct hypothesis [a1,ps,bkg]')
  exit()
if cuts_state not in ('cuts_applied', 'no_cut'):
  print('Please enter correct cuts_state [cuts_applied,no_cut]')
  exit()

if (cuts_state == 'no_cut' and hypo == 'bkg'):
  exit()

print(f'##### Start plotting for {cuts_state} scenario and {hypo} hypothesis')

data_tags = {
    'mcfm': 'JHUGen/MCFM (LO QCD)',
    'phantom': 'Phantom (LO QCD)',
    'jhugenonly': 'JHUGen on-shell (LO QCD)',
    'puremela': 'POWHEG+JHUGen unmerged',
    'mela': 'POWHEG+JHUGen merged'
    }
regions = ('ONSHELL', r'$124.95 < m_{4\ell} < 125.05~\mathrm{GeV}$', get_ONSHELL), ('OFFSHELL', r'$250 < m_{4\ell} < 500~\mathrm{GeV}$', get_OFFSHELL)

if cuts_state == 'no_cut':
  [data_tags.pop(key) for key in ('mcfm', 'phantom')]
  regions = regions[0:1]
elif (cuts_state == 'cuts_applied'):
  if hypo == 'bkg':
    data_tags.pop('jhugenonly')
  elif hypo == 'ps':
    data_tags.pop('phantom')

processes = ('VBF', r'$m_{jj} > 130~\mathrm{GeV}$', get_VBF), ('ZH', r'$80 < m_{jj} < 100~\mathrm{GeV}$', get_ZH), ('WH', r'$70 < m_{jj} < 90~\mathrm{GeV}$', get_WH)
color = {'mcfm': 'gold', 'mela': 'magenta', 'jhugenonly': 'green', 'phantom': 'blue', 'puremela': 'magenta'}
data = {}
print('Loading data...')
for tag in data_tags:
  chain = ROOT.TChain('tree')
  chain.Add(f'{cuts_state}/{tag}_{hypo}.root')
  df = ROOT.RDataFrame(chain)
  data[tag] = df.AsNumpy()
  print(f'   {tag} data loaded.')

print('Finished Loading data.')

pi, offset = np.pi, (2*np.pi)/18
linspace = np.linspace
vars_ = {
    'costhetastar': (array('d', linspace(-1, 1.1, 22)), linspace(-1, 1, 11), r'$\cos(\theta^*)$'),
    'costheta1':    (array('d', linspace(-1, 1.1, 22)), linspace(-1, 1, 11), r'$\cos(\theta_1)$'),
    'costheta2':    (array('d', linspace(-1, 1.1, 22)), linspace(-1, 1, 11), r'$\cos(\theta_2)$'),
    'Phi':          (array('d', linspace(-pi, pi+offset, 21)), linspace(-3.2, 3.2, 9), r'$\Phi$'),
    'Phi1':         (array('d', linspace(-pi, pi+offset, 21)), linspace(-3.2, 3.2, 9), r'$\Phi_1$'),
    'rapidity4lpp': (array('d', linspace(-3, 3.3, 22)), linspace(-3, 3, 13), r'$y_{4\ell+pp}$'),
    'q1':           (array('d', linspace(0, 512.5, 42)), linspace(0, 500, 11), r'$\sqrt{-q_1^2}$'),
    'q2':           (array('d', linspace(0, 512.5, 42)), linspace(0, 500, 11), r'$\sqrt{-q_2^2}$'),
    'mjj':          (array('d', linspace(0, 3075, 42)), np.linspace(0, 3000, 13), r'$m_{jj}$'),
    'm4lpp':        (array('d', linspace(200, 4100, 40)), np.array([200, 400, 600, 1000, 1500, 2000, 2500, 3000, 3500, 4000]), r'$m_{4\ell+pp}$')
    }

figsize = (20, 20)
box = Bbox([[-0.1, -2.3], [20, 18.7]])
def_fs = 32
font = {'weight' : 'bold',
    'size'   : def_fs}
rc('font', **font)
rc('text', usetex=True)
plt.rcParams['axes.grid'] = True
common_vars = 'mjj', 'm4lpp', 'rapidity4lpp'

hypo_tag = {'a1': 'SM signal', 'ps': 'PS signal ($0^{-}$)', 'bkg': 'SM bkg'}
plot_dir = '/user/mmahdavi/public_html/Analysis/anomalous/comparison/helicity_angles'

print('Plotting histograms...')
for process, mjjcut_label, get_process in processes:
  process_tag = '$\mathrm{' + process + '}$' + r'$\rightarrow 4\ell+pp~(\ell = e, \mu)$ ' + f', {hypo_tag[hypo]}'
  for region, region_label, get_region in regions:
    for var, (bins, xticks, var_label) in vars_.items():
      if (var == 'q2' or var == 'm4lpp') and process != 'VBF':
        continue
      if var == 'mjj':
        if process == 'ZH':
          bins, xticks = (array('d', linspace(80, 101, 22)), np.linspace(80, 100, 11))
        if process == 'WH':
          bins, xticks = (array('d', linspace(70, 91, 22)), np.linspace(70, 90, 11))
      if var == 'q1':
        if (process == 'ZH' or process == 'WH') and region == 'ONSHELL':
          bins, xticks = (array('d', linspace(200, 1025, 34)), np.linspace(200, 1000, 9))
        if (process == 'ZH' or process == 'WH') and region == 'OFFSHELL':
          bins, xticks = (array('d', linspace(300, 1025, 30)), np.linspace(300, 1000, 8))
      widths = [bins[i+1] - bins[i] for i in range(0, len(bins)-1)]
      fig, ax = plt.subplots(1, 1, figsize=(figsize))
      xticks = xticks if not ('Phi' in var) else ([-3.14] + list(xticks[1:-1]) + [3.14])
      ax.set_xscale('linear')
      ax.set_xticks(xticks)
      ax.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
      ymax = 0
      for tag, label in data_tags.items():
        if tag == 'jhugenonly' and region != 'ONSHELL':
          continue
        select_process = get_process(tag)
        select_region = get_region(tag)
        selection = select_process & select_region
        var_tag = var if (var in common_vars) else (('vbf_' if process == 'VBF' else 'vh_') + var)
        x, weights = data[tag][var_tag][selection], data[tag]['weight'][selection]
        use_bins = np.concatenate([bins, np.array([max(x)])]) if ((var == 'mjj' and process == 'VBF') or var == 'q1') else bins
        if use_bins[-1] < bins[-1]:
          use_bins = np.concatenate([bins, np.array(2*[bins[-1]])])
        hist, hbins = np.histogram(x, weights=weights, bins=use_bins, density=True)
        if (var == 'mjj' and process == 'VBF') or var == 'q1':
          hist[-3] += hist[-2] + hist[-1]
          hist, hbins = hist[:-1], hbins[:-1]
          hist[-1] = 0
        linestyle = '-' if tag != 'puremela' else '--'
        ax.step(hbins[0:-1], hist, where='post', color=color[tag], label=label, linestyle=linestyle)
        ymax = max(ymax, max(hist))

      ymin, ymax = 0, 1.2 * ymax
      cmstext = plt.text(0.005, 1.03, r'\textbf{CMS}', fontsize =def_fs+16, transform=ax.transAxes)
      plt.text(0.12, 1.032, r'\textit{Simulation}', fontsize=def_fs+12, transform=ax.transAxes)
      plt.text(0.86, 1.032, r'13 TeV', fontsize=def_fs+12, transform=ax.transAxes)
      props = dict(boxstyle='round', facecolor='white', alpha=1, ec='white')
      process_handles = [mpl_patches.Rectangle((0, 0), 1, 1, fc='white', ec='white', lw=0, alpha=0)]
      process_tag_text = [process_tag]
      process_leg = plt.legend(process_handles, process_tag_text, loc='best', fontsize=def_fs+4, fancybox=False, framealpha=0, handlelength=0, handletextpad=0)
      text1 = plt.text(0.01, -0.10, 'Constraints:', fontsize =def_fs+4, transform=ax.transAxes, bbox=props)
      text2 = plt.text(0.01, -0.15, mjjcut_label, fontsize =def_fs+2, transform=ax.transAxes, bbox=props)
      text3 = plt.text(0.01, -0.20, region_label, fontsize =def_fs+2, transform=ax.transAxes, bbox=props)
      text4 = r'w/ ' if cuts_state == 'cuts_applied' else r'w/o '
      text4 = plt.text(0.01, -0.25, text4+r'sel. reqs. on $\ell,~q$', fontsize =def_fs+2, transform=ax.transAxes, bbox=props)
      
      legend_pos = (1.01, -0.17) 
      legend = plt.legend(loc='center right', bbox_to_anchor=legend_pos, fontsize=def_fs-2, ncol= 1, frameon=False)
      legend.get_frame().set_alpha(1)
      plt.gca().add_artist(legend)
      plt.gca().add_artist(process_leg)
      xmin = xticks[0]
      xmax = xticks[-1]
      plt.xlim(xmin, xmax)
      plt.ylim(ymin, ymax)
      var_label = r'$m_{4\ell+pp}$' if (var == 'q1' and process != 'VBF') else var_label
      plt.xlabel(var_label, fontsize=def_fs+16, labelpad=20)
      ax.tick_params('y', length=20, width=2, which='major')
      ax.tick_params('x', length=10, width=2, which='major')
      root_dir = f'{plot_dir}/{cuts_state}/{hypo}/{process}'
      os.makedirs(f'{root_dir}', exist_ok=True)
      out_file = f'{root_dir}/{process}_{region}_{var}_{hypo}'
      fig.savefig(out_file + '.png', bbox_extra_artists=(process_leg, legend, cmstext, text1, text2, text3, text4), bbox_inches=box)
      plt.close()

print('##### All are Finished.')

