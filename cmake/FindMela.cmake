find_path(
  MelaCore_INCLUDE_DIR Mela.h
  HINTS "$ENV{MELA_ROOT_DIR}/interface"
)
set(MelaCore_LIBRARY_DIR_HINT "$ENV{MELA_ROOT_DIR}/data/$ENV{SCRAM_ARCH}")
find_library(
  MelaCore_JHUGenMELAMELA_LIBRARY JHUGenMELAMELA
  HINTS "${MelaCore_LIBRARY_DIR_HINT}"
)
find_library(
  MelaCore_collier_LIBRARY collier
  HINTS "${MelaCore_LIBRARY_DIR_HINT}"
  NO_DEFAULT_PATH  # Ignore the library in the LCG environment
)
find_library(
  MelaCore_mcfm_LIBRARY NAMES mcfm_707 mcfm
  HINTS "${MelaCore_LIBRARY_DIR_HINT}"
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Mela
  REQUIRED_VARS
    MelaCore_INCLUDE_DIR MelaCore_JHUGenMELAMELA_LIBRARY
    MelaCore_collier_LIBRARY MelaCore_mcfm_LIBRARY
)

mark_as_advanced(
    Mela_FOUND MelaCore_INCLUDE_DIR MelaCore_JHUGenMELAMELA_LIBRARY
    MelaCore_collier_LIBRARY MelaCore_mcfm_LIBRARY
)

if(Mela_FOUND AND NOT TARGET Mela)
  add_library(Mela::MelaCore::Mela SHARED IMPORTED)
  set_target_properties(Mela::MelaCore::Mela PROPERTIES
    IMPORTED_LOCATION "${MelaCore_JHUGenMELAMELA_LIBRARY}"
    INTERFACE_INCLUDE_DIRECTORIES "${MelaCore_INCLUDE_DIR}"
  )

  add_library(Mela::MelaCore::collier SHARED IMPORTED)
  set_target_properties(Mela::MelaCore::collier PROPERTIES
    IMPORTED_LOCATION "${MelaCore_collier_LIBRARY}"
    INTERFACE_INCLUDE_DIRECTORIES "${MelaCore_INCLUDE_DIR}"
  )

  add_library(Mela::MelaCore::mcfm SHARED IMPORTED)
  set_target_properties(Mela::MelaCore::mcfm PROPERTIES
    IMPORTED_LOCATION "${MelaCore_mcfm_LIBRARY}"
    INTERFACE_INCLUDE_DIRECTORIES "${MelaCore_INCLUDE_DIR}"
  )

  add_library(Mela INTERFACE)
  target_link_libraries(Mela
    INTERFACE
      Mela::MelaCore::Mela Mela::MelaCore::collier Mela::MelaCore::mcfm
  )
endif()
