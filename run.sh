show_progress() {
  echo -en "\rsubmitting jobs    [$1 %]"
  sleep 0.5
  echo -en "\rsubmitting jobs.   [$1 %]"
  sleep 0.5
  echo -en "\rsubmitting jobs..  [$1 %]"
  sleep 0.5
  echo -en "\rsubmitting jobs... [$1 %]"
  sleep 0.5
}

hypo=$1
if [[ $hypo != "a1" ]] && [[ $hypo != "ps" ]] && [[ $hypo != "bkg" ]]; then
  echo "Please specify the hypothesis correctly [a1,ps,bkg]"
  exit 1
fi

mkdir -p outputs merged
rm -f output/*_${hypo}.root

if [[ $hypo = "a1" ]];then
  show_progress 0
  computeAnglesNLO --output=outputs/4lvbf_mela_${hypo}.root --hypothesis=$hypo $(cat files/4lvbf_mela_files) &> /dev/null &
  show_progress 7
  computeAnglesPureNLO --output=outputs/4lvbf_puremela_${hypo}.root --hypothesis=$hypo $(cat files/4lvbf_mela_files) &> /dev/null &
  show_progress 14
  computeAnglesNLO --output=outputs/4lzh_mela_${hypo}.root --hypothesis=$hypo $(cat files/4lzh_mela_files) &> /dev/null &
  show_progress 21
  computeAnglesPureNLO --output=outputs/4lzh_puremela_${hypo}.root --hypothesis=$hypo $(cat files/4lzh_mela_files) &> /dev/null &
  show_progress 28
  computeAngles --output=merged/mcfm_${hypo}.root $(cat files/4lvbf_mcfm_${hypo}_files) &> /dev/null &
  show_progress 35
  computeAngles --output=outputs/4lwh_jhugenonly_${hypo}.root $(cat files/4lwh_jhugenonly_${hypo}_files) &> /dev/null &
  show_progress 42
  computeAngles --output=outputs/4lzh_jhugenonly_${hypo}.root $(cat files/4lzh_jhugenonly_${hypo}_files) &> /dev/null &
  show_progress 49
  computeAngles --output=outputs/4lvbf_jhugenonly_${hypo}.root $(cat files/4lvbf_jhugenonly_${hypo}_files) &> /dev/null &
  show_progress 56
  computeAnglesNLO --output=outputs/4lwph_mela_${hypo}.root --hypothesis=$hypo $(cat files/4lwph_mela_files) &> /dev/null &
  show_progress 63
  computeAnglesNLO --output=outputs/4lwmh_mela_${hypo}.root --hypothesis=$hypo $(cat files/4lwmh_mela_files) &> /dev/null &
  show_progress 70
  computeAnglesPureNLO --output=outputs/4lwph_puremela_${hypo}.root --hypothesis=$hypo $(cat files/4lwph_mela_files) &> /dev/null &
  show_progress 77
  computeAnglesPureNLO --output=outputs/4lwmh_puremela_${hypo}.root --hypothesis=$hypo $(cat files/4lwmh_mela_files) &> /dev/null &
  show_progress 84
  computeAngles --output=outputs/4evbf_phantom_${hypo}.root $(cat files/4evbf_phantom_${hypo}_files) &> /dev/null &
  show_progress 92
  computeAngles --output=outputs/4muvbf_phantom_${hypo}.root $(cat files/4muvbf_phantom_${hypo}_files) &> /dev/null &
fi

if [[ $hypo = "bkg" ]];then
  show_progress 0
  computeAnglesNLO --output=outputs/4lvbf_mela_${hypo}.root --hypothesis=$hypo $(cat files/4lvbf_mela_files) &> /dev/null &
  show_progress 9
  computeAnglesPureNLO --output=outputs/4lvbf_puremela_${hypo}.root --hypothesis=$hypo $(cat files/4lvbf_mela_files) &> /dev/null &
  show_progress 18
  computeAnglesNLO --output=outputs/4lzh_mela_${hypo}.root --hypothesis=$hypo $(cat files/4lzh_mela_files) &> /dev/null &
  show_progress 27
  computeAnglesPureNLO --output=outputs/4lzh_puremela_${hypo}.root --hypothesis=$hypo $(cat files/4lzh_mela_files) &> /dev/null &
  show_progress 36
  computeAngles --output=merged/mcfm_${hypo}.root $(cat files/4lvbf_mcfm_${hypo}_files) &> /dev/null &
  show_progress 45
  computeAnglesNLO --output=outputs/4lwph_mela_${hypo}.root --hypothesis=$hypo $(cat files/4lwph_mela_files) &> /dev/null &
  show_progress 54
  computeAnglesNLO --output=outputs/4lwmh_mela_${hypo}.root --hypothesis=$hypo $(cat files/4lwmh_mela_files) &> /dev/null &
  show_progress 63
  computeAnglesPureNLO --output=outputs/4lwph_puremela_${hypo}.root --hypothesis=$hypo $(cat files/4lwph_mela_files) &> /dev/null &
  show_progress 72
  computeAnglesPureNLO --output=outputs/4lwmh_puremela_${hypo}.root --hypothesis=$hypo $(cat files/4lwmh_mela_files) &> /dev/null &
  show_progress 81
  computeAngles --output=outputs/4evbf_phantom_${hypo}.root $(cat files/4evbf_phantom_${hypo}_files) &> /dev/null &
  show_progress 99
  computeAngles --output=outputs/4muvbf_phantom_${hypo}.root $(cat files/4muvbf_phantom_${hypo}_files) &> /dev/null &
fi

if [[ $hypo = "ps" ]];then
  show_progress 0
  computeAnglesNLO --output=outputs/4lvbf_mela_${hypo}.root --hypothesis=$hypo $(cat files/4lvbf_mela_files) &> /dev/null &
  show_progress 8
  computeAnglesPureNLO --output=outputs/4lvbf_puremela_${hypo}.root --hypothesis=$hypo $(cat files/4lvbf_mela_files) &> /dev/null &
  show_progress 16
  computeAnglesNLO --output=outputs/4lzh_mela_${hypo}.root --hypothesis=$hypo $(cat files/4lzh_mela_files) &> /dev/null &
  show_progress 24
  computeAnglesPureNLO --output=outputs/4lzh_puremela_${hypo}.root --hypothesis=$hypo $(cat files/4lzh_mela_files) &> /dev/null &
  show_progress 32
  computeAngles --output=merged/mcfm_${hypo}.root $(cat files/4lvbf_mcfm_${hypo}_files) &> /dev/null &
  show_progress 40
  computeAngles --output=outputs/4lwh_jhugenonly_${hypo}.root $(cat files/4lwh_jhugenonly_${hypo}_files) &> /dev/null &
  show_progress 48
  computeAngles --output=outputs/4lzh_jhugenonly_${hypo}.root $(cat files/4lzh_jhugenonly_${hypo}_files) &> /dev/null &
  show_progress 56
  computeAngles --output=outputs/4lvbf_jhugenonly_${hypo}.root $(cat files/4lvbf_jhugenonly_${hypo}_files) &> /dev/null &
  show_progress 66
  computeAnglesNLO --output=outputs/4lwph_mela_${hypo}.root --hypothesis=$hypo $(cat files/4lwph_mela_files) &> /dev/null &
  show_progress 76
  computeAnglesNLO --output=outputs/4lwmh_mela_${hypo}.root --hypothesis=$hypo $(cat files/4lwmh_mela_files) &> /dev/null &
  show_progress 88
  computeAnglesPureNLO --output=outputs/4lwph_puremela_${hypo}.root --hypothesis=$hypo $(cat files/4lwph_mela_files) &> /dev/null &
  show_progress 96
  computeAnglesPureNLO --output=outputs/4lwmh_puremela_${hypo}.root --hypothesis=$hypo $(cat files/4lwmh_mela_files) &> /dev/null &
fi

space="   "
echo -e "\rAll jobs are submitted. [100 %]$space"

while [[ ! -z $(jobs | grep Running) ]]; do
  sleep 0.2
  echo -en "\r$(jobs | grep Running | wc -l) of jobs are not finished yet... \\$space"
  sleep 0.2
  echo -en "\r$(jobs | grep Running | wc -l) of jobs are not finished yet... |$space"
  sleep 0.2
  echo -en "\r$(jobs | grep Running | wc -l) of jobs are not finished yet... /$space"
  sleep 0.2
  echo -en "\r$(jobs | grep Running | wc -l) of jobs are not finished yet... -$space"
done

echo -e "\rAll jobs are finished successfully ;)$space"
echo "Merging files..."

if [[ $hypo = "ps" ]] || [[ $hypo = "a1" ]];then
  hadd -f merged/jhugenonly_${hypo}.root outputs/{4lvbf,4lwh,4lzh}_jhugenonly_${hypo}.root &> /dev/null
fi

if [[ $hypo = "bkg"  ]] || [[ $hypo = "a1" ]];then
  hadd -f merged/phantom_${hypo}.root    outputs/{4e,4mu}vbf_phantom_${hypo}.root &> /dev/null
fi

hadd -f merged/mela_${hypo}.root       outputs/{4lvbf,4lwmh,4lwph,4lzh}_mela_${hypo}.root &> /dev/null
hadd -f merged/puremela_${hypo}.root   outputs/{4lvbf,4lwmh,4lwph,4lzh}_puremela_${hypo}.root &> /dev/null

echo "Finished merging."

