#include <iostream>

#include <boost/program_options.hpp>

#include <TChain.h>
#include <TFile.h>
#include <TTree.h>
#include <TTreeReader.h>
#include <TTreeReaderArray.h>

#include <PDGHelpers.h>
#include <TUtil.hh>


namespace po = boost::program_options;
namespace PDG = PDGHelpers;


/* A function to sort final state leptons w.r.t the finding best pair and the
 * second pair. Also in each pair order them as particle then anti-particle.
 */
std::array<int, 4> GetSortedLeptonsIndices(
    std::vector<int> ids,
    std::vector<TLorentzVector> const &p4s
    ) {
  int l1 = -1, l2 = -1, l3 = -1, l4 = -1;
  float smallestDiff = 1.e10;
  for (int i = 0; i < 4; i++) {
    for (int j = i + 1; j < 4; j++) {
      auto idi = ids[i], idj = ids[j];
      if (idi * idj > 0)
        continue;
      if (std::abs(idi) != std::abs(idj))
        continue;

      float diff = std::fabs((p4s.at(i) + p4s.at(j)).M() - PDG::Zmass);
      if (diff < smallestDiff) {
        if (idi > 0) {
          l1 = i;
          l2 = j;
        } else {
          l1 = j;
          l2 = i;
        }
        smallestDiff = diff;
      }
    }
  }

  for (int i = 0; i < 4; i++) {
    if (i == l1 or i == l2)
      continue;
    if (ids[i] > 0)
      l3 = i;
    else
      l4 = i;
  }

  // if still one of l1, ..., l4 is -1 ==> there are more than 2 partiles with
  // the same charge. ==> event must be rejected ==> return invalid values.
  if (l1 < 0 or l2 < 0 or l3 < 0 or l4 < 0)
    return {-1, -1, -1, -1};

  // check all pairs have invariant mass > 4, if not return invalid values.
  if ((p4s.at(l3) + p4s.at(l4)).M() <= 4 or
    (p4s.at(l1) + p4s.at(l2)).M() <= 4)
    return {-1, -1, -1, -1};

  return {l1, l2, l3, l4};
}


// A function to guess the q q_bar --> Z --> ZH process
bool hasZHTopology(int id1, int id2) {
  return id1 == -id2;
}


/* A function to guess the q q`_bar --> W --> WH process.
 * For this both q and q` can not be Down or Up type qurks at the same time.
 */
bool hasWHTopology(int id1, int id2) {
  if (id1 * id2 > 0)
    return false;

  return (
      (PDG::isUpTypeQuark(id1) and PDG::isDownTypeQuark(id2)) or
      (PDG::isUpTypeQuark(id2) and PDG::isDownTypeQuark(id1))
      );
}


inline float dR(TLorentzVector const &p1, TLorentzVector const &p2) {
  return std::sqrt(
      std::pow(p1.Eta() - p2.Eta(), 2) +
      std::pow(p1.Phi() - p2.Phi(), 2)
      );
}


bool greater(const std::pair<float, int> &l,
    const std::pair<float, float> &r) {
  return (l.first > r.first);
}


std::array<int, 2> GetPtSortedIndices(std::vector<TLorentzVector> const &p4s) {
  std::vector<std::pair<float, int>> pts;
  for (size_t i = 0; i < p4s.size(); i++)
    pts.emplace_back(p4s[i].Pt(), i);

  std::sort(pts.begin(), pts.end(), greater);
  return {pts[0].second, pts[1].second};
}


// Main function of the program
int main(int argc, char **argv) {
  // Defining needed variables for the program arguments.
  bool isVerbose;
  std::string output, hypo;
  std::vector<std::string> inputs;
  int firstEvent, lastEvent;


  // Managing program options.
  po::options_description optionsDescription{"Options"};
  optionsDescription.add_options()
    ("help,h", "Help screen")
    ("output,o", po::value<std::string>(&output)->required(), "Output file")
    ("first-event,f", po::value<int>(&firstEvent)->default_value(1),
     "Fist event (number) to start")
    ("last-event,l", po::value<int>(&lastEvent)->default_value(-1),
     "Last event (number)")
    ("hypothesis", po::value<std::string>(&hypo)->default_value("a1"),
     "Specifies the hypotheis")
    ("verbose,v", "Show progress");

  po::options_description hiddenOptionsDescription;
  hiddenOptionsDescription.add_options()
    ("input-files", po::value<std::vector<std::string>>(), "");
  po::positional_options_description posOptionsDescription;
  posOptionsDescription.add("input-files", -1);

  po::options_description allOptionsDescription;
  allOptionsDescription.add(optionsDescription).add(hiddenOptionsDescription);

  po::variables_map options;
  po::store(
      po::command_line_parser(argc, argv).options(allOptionsDescription)
      .positional(posOptionsDescription).run(),
      options);

  if (options.count("help")) {
    std::cerr << "Usage:" << std::endl
      << "mela INPUT_FILE1 [INPUT_FILE2 [...]] [OPTIONS]" << std::endl;
    std::cerr << optionsDescription << std::endl;
    return EXIT_SUCCESS;
  }

  isVerbose = options.count("verbose");
  po::notify(options);

  if (hypo == "a1")
    hypo = "p_Gen_JJEW_SIG_ghv1_1_MCFM";
  else if (hypo == "ps")
    hypo = "p_Gen_JJEW_SIG_ghv4_1_MCFM";
  else
    hypo = "p_Gen_JJEW_BKG_MCFM";

  // If no input file is provided throw an exception.
  if (options["input-files"].empty()) {
    std::ostringstream message;
    message << "No input file is provided. Please pass them as POSITIONAL "
      << "arguments.";
    throw std::runtime_error(message.str());
  }

  inputs = options["input-files"].as<std::vector<std::string>>();
  if( lastEvent == -1)
    lastEvent = std::numeric_limits<int>::max();
  // END of Managing program options.


  // output varibales
  float weight, mjj, m4l, detajj, m4lpp, rapidity4lpp,
        vbf_costhetastar, vh_costhetastar, vbf_costheta1, vh_costheta1,
        vbf_costheta2, vh_costheta2, vbf_Phi, vh_Phi, vbf_Phi1, vh_Phi1,
        vbf_q1, vh_q1, vbf_q2, vh_q2,
        vbf_q2_1, vbf_q2_2;
  int isZH = 0, isWH = 0;

  TFile outputFile(output.c_str(), "RECREATE");
  TTree tree("tree", "tree");
  tree.SetDirectory(nullptr);
  tree.SetDirectory(&outputFile);
  // Common branches
  tree.Branch("weight", &weight);
  tree.Branch("mjj", &mjj);
  tree.Branch("m4l", &m4l);
  tree.Branch("detajj", &detajj);
  tree.Branch("m4lpp", &m4lpp);
  tree.Branch("rapidity4lpp", &rapidity4lpp);
  // VBF branches
  tree.Branch("vbf_costhetastar", &vbf_costhetastar);
  tree.Branch("vbf_costheta1", &vbf_costheta1);
  tree.Branch("vbf_costheta2", &vbf_costheta2);
  tree.Branch("vbf_Phi", &vbf_Phi);
  tree.Branch("vbf_Phi1", &vbf_Phi1);
  tree.Branch("vbf_q1", &vbf_q1);
  tree.Branch("vbf_q2", &vbf_q2);
  // VH branches
  tree.Branch("isZH", &isZH);
  tree.Branch("isWH", &isWH);
  tree.Branch("vh_costhetastar", &vh_costhetastar);
  tree.Branch("vh_costheta1", &vh_costheta1);
  tree.Branch("vh_costheta2", &vh_costheta2);
  tree.Branch("vh_Phi", &vh_Phi);
  tree.Branch("vh_Phi1", &vh_Phi1);
  tree.Branch("vh_q1", &vh_q1);
  tree.Branch("vh_q2", &vh_q2);

  // Adding input files to the chain.
  TChain chain("SkimTree");
  TTreeReader reader(&chain);
  for (auto &file : inputs)
    chain.Add(file.c_str());

  if (isVerbose)
    std::cout << "Input root files are added into the chain." << std::endl;

  int const nEvents = chain.GetEntries();
  lastEvent = (nEvents < lastEvent) ? nEvents : lastEvent;
  if (isVerbose)
    std::cout << (lastEvent - firstEvent + 1) << " event(s) will be processed."
      << std::endl;

  int eventNum = firstEvent - 1, localEventNum = 0;

  // Reading needed branches from input files
  TTreeReaderArray<Float_t> lhePx{reader, "lheparticles_px"};
  TTreeReaderArray<Float_t> lhePy{reader, "lheparticles_py"};
  TTreeReaderArray<Float_t> lhePz{reader, "lheparticles_pz"};
  TTreeReaderArray<Float_t> lheE{reader, "lheparticles_E"};
  TTreeReaderArray<Int_t> lhePdgId{reader, "lheparticles_id"};
  TTreeReaderArray<Int_t> lheStatus{reader, "lheparticles_status"};
  TTreeReaderValue<Bool_t> isInvalidWeight{reader, "invalidReweightingWgts"};
  TTreeReaderValue<Float_t> eventWeight{reader, "event_wgt"};
  TTreeReaderValue<Float_t> sampleWeight{reader, "sample_wgt"};
  TTreeReaderValue<Float_t> adjustor{reader, "event_wgt_adjustment_NNPDF30"};
  TTreeReaderValue<Float_t> hypoWeight{reader, hypo.c_str()};
  TTreeReaderValue<Float_t> cpsToBW{reader, "p_Gen_CPStoBWPropRewgt"};

  int hadTauEvents = 0;

  /*** Main Loop ***/
  while (reader.SetEntry(++eventNum) == TTreeReader::kEntryValid) {
    if (eventNum > lastEvent)
      break;
    localEventNum++;

    if (isVerbose and (localEventNum % 250000) == 0) {
      std::cout << localEventNum << " events are processed. " << std::endl;
    }

    // Rejecting events having Tau lepton in final state
    bool foundTau = false;
    for (auto &id : lhePdgId) {
      if (PDG::isATauLepton(id)) {
        foundTau = true;
        hadTauEvents++;
        break;
      }
    } if (foundTau) continue;

    // Categorizing particles into mother, daughter and associated jets.
    std::vector<int> leptonIds, incomingIds, outgoingIds;
    std::vector<TLorentzVector> leptonP4, incomingP4, outgoingP4;
    // iterating over lhe particles.
    for (std::size_t i = 0; i < lhePdgId.GetSize(); i++) {
      auto pdgId = lhePdgId[i];
      auto status = lheStatus[i];
      if (PDG::isALepton(pdgId) and status != -1) {
        TLorentzVector p4;
        p4.SetPxPyPzE(lhePx[i], lhePy[i], lhePz[i], lheE[i]);
        if (p4.Pt() < 7 or std::fabs(p4.Eta()) > 2.4)
          continue;
        leptonIds.push_back(pdgId);
        leptonP4.push_back(p4);
      } else if ((PDG::isAQuark(pdgId) or PDG::isAGluon(pdgId)) and
          status == -1) {
        auto id = (PDG::isAGluon(pdgId)) ? 0 : pdgId;
        incomingIds.push_back(id);
        TLorentzVector p4;
        p4.SetPxPyPzE(lhePx[i], lhePy[i], lhePz[i], lheE[i]);
        incomingP4.push_back(p4);
      } else if ((PDG::isAQuark(pdgId) or PDG::isAGluon(pdgId)) and
          status != -1) {
        TLorentzVector p4;
        p4.SetPxPyPzE(lhePx[i], lhePy[i], lhePz[i], lheE[i]);
        if (p4.Pt() < 30 or std::fabs(p4.Eta()) > 4.7)
          continue;
        outgoingP4.push_back(p4);
        auto id = (PDG::isAGluon(pdgId)) ? 0 : pdgId;
        outgoingIds.push_back(id);
      }
    }

    if (outgoingIds.size() < 2 or
        incomingIds.size() < 2 or
        leptonIds.size() != 4) {
      continue;
    }

    auto [l1, l2, l3, l4] = GetSortedLeptonsIndices(leptonIds, leptonP4);
    // if there is a ll pairs with invariant mass -< 4  or there are more than 2
    // leptons with the same charge ==> rejecting the event
    if (l1 < 0)
      continue;

    // The event with outgoing quarks having not enough separation is rejected.
    if (dR(outgoingP4[0], outgoingP4[1]) < 0.4)
      continue;

    auto [o1, o2] = GetPtSortedIndices(outgoingP4);
    auto &jet1 = outgoingP4[o1];
    auto &jet2 = outgoingP4[o2];
    auto jet1Id = outgoingIds[o1];
    auto jet2Id = outgoingIds[o2];

    bool _1stGt2nd = (incomingP4[0].E() > incomingP4[1].E());
    auto injet1 = (_1stGt2nd) ? incomingP4[0] : incomingP4[1];
    auto injet2 = (not _1stGt2nd) ? incomingP4[0] : incomingP4[1];
    auto injet1Id = (_1stGt2nd) ? incomingIds[0] : incomingIds[1];
    auto injet2Id = (not _1stGt2nd) ? incomingIds[0] : incomingIds[1];

    isZH = hasZHTopology(injet1Id, injet2Id);
    isWH = hasWHTopology(injet1Id, injet2Id);

    auto &p4M11 = leptonP4[l1], &p4M12 = leptonP4[l2],
         &p4M21 = leptonP4[l3], &p4M22 = leptonP4[l4];
    auto Z1_lept1Id = leptonIds[l1], Z1_lept2Id = leptonIds[l2],
         Z2_lept1Id = leptonIds[l3], Z2_lept2Id = leptonIds[l4];

    // assign values to all outputs
    if (*isInvalidWeight)
      weight = 0;
    else {
      float absAdjustor = std::fabs(*adjustor);
      float adjusted = (absAdjustor > 10) ? (10. / absAdjustor) : (*adjustor);
      weight = adjusted * (*cpsToBW) * (*hypoWeight)
        * (*sampleWeight) * (*eventWeight);
    }

    auto const p4jj = outgoingP4[0] + outgoingP4[1];
    auto const p44l = leptonP4[0] + leptonP4[1] + leptonP4[2] + leptonP4[3];
    auto const p4jj4l = p4jj + p44l;
    detajj = std::fabs(outgoingP4[0].Eta() - outgoingP4[1].Eta());
    mjj = p4jj.M();
    m4l = p44l.M();
    m4lpp = p4jj4l.M();
    rapidity4lpp = p4jj4l.Rapidity();

    TUtil::computeVBFAngles(
        vbf_costhetastar, vbf_costheta1, vbf_costheta2, vbf_Phi, vbf_Phi1,
        vbf_q2_1, vbf_q2_2,
        p4M11, Z1_lept1Id,
        p4M12, Z1_lept2Id,
        p4M21, Z2_lept1Id,
        p4M22, Z2_lept2Id,
        jet1, jet1Id,
        jet2, jet2Id,
        &injet1, injet1Id,
        &injet2, injet2Id
        );
    vbf_q1 = std::sqrt(vbf_q2_1);
    vbf_q2 = std::sqrt(vbf_q2_2);

    TUtil::computeVHAngles(
        vh_costhetastar, vh_costheta1, vh_costheta2, vh_Phi, vh_Phi1,
        vh_q1, vh_q2,
        p4M11, Z1_lept1Id,
        p4M12, Z1_lept2Id,
        p4M21, Z2_lept1Id,
        p4M22, Z2_lept2Id,
        jet1, jet1Id,
        jet2, jet2Id,
        &injet1, injet1Id,
        &injet2, injet2Id
        );

    tree.Fill();
  }
  /*** END of Main Loop ***/


  if (isVerbose) {
    std::cout << "All " << (lastEvent - firstEvent + 1) <<
      " events are processed successfully.\n";
    std:: cout << hadTauEvents
      << " (" << (100. * hadTauEvents) / lastEvent << " %) of "
      << "events are rejected due to having Tau lepton(s) in final state.\n";
  }

  outputFile.Write();
  outputFile.Close();
  return EXIT_SUCCESS;
}
